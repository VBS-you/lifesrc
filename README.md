# lifesrc

Lifesrc is David Bell's Life search program for finding new oscillators and spaceships. It is a C implementation of an algorithm developed by Dean Hickerson in 6502 assembler. Many of the well-known oscillators and non-standard spaceships in Life 